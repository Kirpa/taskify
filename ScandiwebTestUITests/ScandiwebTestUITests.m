//
//  ScandiwebTestUITests.m
//  ScandiwebTestUITests
//
//  Created by Vadim Zhepetov on 22/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SharedConstants.h"

@interface ScandiwebTestUITests : XCTestCase

@property (nonatomic) XCUIApplication *app;

@end

@implementation ScandiwebTestUITests

- (void)setUp {
    [super setUp];
        self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    self.app = [[XCUIApplication alloc] init];
    self.app.launchArguments = [self.app.launchArguments arrayByAddingObject:kIsUITestingFlag];
    [self.app launch];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)createTaskWithDescription:(NSString *)description duration:(NSString *)duration
{
    [self.app.buttons[@"ShowEditorButton"] tap];
    
    XCUIElement *editorviewdescriptionfieldTextField = self.app.textFields[@"EditorViewDescriptionField"];
    XCTAssertTrue(editorviewdescriptionfieldTextField.exists);
    [editorviewdescriptionfieldTextField tap];
    [editorviewdescriptionfieldTextField typeText:description];
    
    XCUIElement *editorviewtimespentfieldTextField = self.app.textFields[@"EditorViewTimeSpentField"];
    XCTAssertTrue(editorviewtimespentfieldTextField.exists);
    [editorviewtimespentfieldTextField tap];
    [editorviewtimespentfieldTextField typeText:duration];
    [self.app.buttons[@"EditorViewConfirmButton"] tap];
}

- (void)testAddingTask
{
    XCTAssertEqual(self.app.tables.cells.count, 0);
    NSString *testDescription = @"ui_test_task_description";
    NSString *testDuration = @"85";
    NSString *expectedSisplayedTestDuration = @"1:25";
    [self createTaskWithDescription:testDescription duration:testDuration];

    XCUIElementQuery *cells = self.app.tables.cells;
    XCTAssertEqual(cells.count, 1);
    XCUIElement *createdCell = [cells elementBoundByIndex:0];
    XCUIElement *descriptionLabel = createdCell.staticTexts[@"TaskCellSummaryLabel"];
    XCUIElement *timeSpentLabel = createdCell.staticTexts[@"TaskCellTimeSpentLabel"];
    XCTAssertTrue([descriptionLabel.label isEqualToString:testDescription]);
    XCTAssertTrue([timeSpentLabel.label isEqualToString: expectedSisplayedTestDuration]);
}

- (void)testDeletingTask
{
    XCTAssertEqual(self.app.tables.cells.count, 0);
    NSString *testDescription = @"ui_test_task_description";
    NSString *testDuration = @"85";
    [self createTaskWithDescription:testDescription duration:testDuration];
    
    XCUIElementQuery *cells = self.app.tables.cells;
    XCTAssertEqual(cells.count, 1);
    XCUIElement *cell = [cells elementBoundByIndex:0];
    [cell swipeLeft];
    [cell.buttons[@"Delete"] tap];
    XCTAssertEqual(cells.count, 0);
    XCTAssert(!cell.exists);
}

@end
