//
//  TaskModelTest.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 24/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import XCTest
@testable import ScandiwebTest

class TaskModelTest: XCTestCase {
    
    func testNSEquatableIsEqualToSelf() {
        let task1 = TaskModel(summary: "summary1", date: NSDate(), duration: 1)
        
        XCTAssertTrue(task1 == task1)
    }
    
    func testNSEquatableIsEqualToOther() {
        let testDate = NSDate()
        let task1 = TaskModel(summary: "summary1", date: testDate, duration: 1)
        let task2 = TaskModel(summary: "summary1", date: testDate, duration: 1)
        
        XCTAssertTrue(task1 == task2)
    }
    
    func testNSEquatableIsNotEqualToOther() {
        let testDate = NSDate()
        let task1 = TaskModel(summary: "summary1", date: testDate, duration: 1)
        let task2 = TaskModel(summary: "summary2", date: testDate, duration: 1)
        
        XCTAssertFalse(task1 == task2)
    }
    
    func testNSCoding() {
        let testSummary = "test summary"
        let testDate = NSDate()
        let testDuration = NSNumber(value: 1)
        let sut = TaskModel(summary: testSummary, date: testDate, duration: testDuration)
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: sut)
        let decodedSut = NSKeyedUnarchiver.unarchiveObject(with: encodedData)
        
        XCTAssertTrue(sut.isEqual(decodedSut))
    }
}
