//
//  TaskDataSourceTests.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 25/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import XCTest
@testable import ScandiwebTest

class StorageStub: Storage {
    var storedData: [TaskModel]
    init(data: [TaskModel]) {
        self.storedData = data
    }
    
    func readData() -> [TaskModel] {
        return self.storedData
    }
    
    func save(data: [TaskModel]) {}
    func deleteData() {}
}

class TaskDataSourceTests: XCTestCase {
    
    let calendar = Calendar.current
    
    func makeYesterdayTask() -> TaskModel {
        let date = self.calendar.date(byAdding: .day, value: -1, to: Date())!
        return TaskModel(summary: "summary", date: date as NSDate, duration: 1)
    }
    
    func makeTodayTask() -> TaskModel {
        return TaskModel(summary: "summary", date: NSDate(), duration: 1)
    }
    
    func makeTomorrowTask() -> TaskModel {
        let date = self.calendar.date(byAdding: .day, value: 1, to: Date())!
        return TaskModel(summary: "summary", date: date as NSDate, duration: 1)
    }
    
    func makeLongAgoTask() -> TaskModel {
        let date = self.calendar.date(byAdding: .year, value: -3, to: Date())!
        return TaskModel(summary: "summary", date: date as NSDate, duration: 1)
    }
    
    func makeFutureTask() -> TaskModel {
        let date = self.calendar.date(byAdding: .year, value: 3, to: Date())!
        return TaskModel(summary: "summary", date: date as NSDate, duration: 1)
    }
    
    func persistentSut() -> TaskDataSource {
        return TaskDataSource(taskStorageFile: "task_data_source_test_storage")
    }
    
    override func tearDown() {
        self.persistentSut().deletePersistentData()
    }
    
    func testTaskCount() {
        let storage = StorageStub(data: [])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        let task = self.makeTodayTask()
        
        sut.add(task: task)
        sut.add(task: task)
        sut.add(task: task)
        
        XCTAssert(sut.taskCount() == 3)
    }
    
    func testKeepsAddedTask() {
        let storage = StorageStub(data: [])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        let task = self.makeTodayTask()
        
        sut.add(task: task)
        let retrievedTask = sut.task(atIndex: 0)
        
        XCTAssert(task == retrievedTask)
    }
    
    func testReadsTasksFromStorage() {
        let task = self.makeTodayTask()
        let storage = StorageStub(data: [task, task])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let retrievedTask = sut.task(atIndex: 0)
        
        XCTAssert(sut.taskCount() == 2)
        XCTAssert(task == retrievedTask)
    }
    
    func testReplacingTask() {
        let task = self.makeTodayTask()
        let storage = StorageStub(data: [task])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let newTask = self.makeYesterdayTask()
        sut.replace(task: newTask, atIndex: 0)
        let retrievedTask = sut.task(atIndex: 0)
        
        XCTAssert(sut.taskCount() == 1)
        XCTAssert(newTask == retrievedTask)
    }
    
    func testReplacingTaskWithIndexPath() {
        let task = self.makeTodayTask()
        let storage = StorageStub(data: [task])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let newTask = self.makeYesterdayTask()
        sut.replace(task: newTask, atIndexPath:IndexPath(item: 0, section: 0))
        let retrievedTask = sut.task(atIndex: 0)
        
        XCTAssert(sut.taskCount() == 1)
        XCTAssert(newTask == retrievedTask)
    }
    
    func testDeletingTask() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [yesterdayTask, todayTask, tomorrowTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        sut.removeTask(atIndex: 0)
        
        XCTAssert(sut.taskCount() == 2)
        XCTAssert(sut.task(atIndex: 0) == todayTask)
    }
    
    func testShouldCountDaysWhenSeveralOneDayTaskAdded() {
        let task = self.makeTodayTask()
        let storage = StorageStub(data: [task, task, task])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        XCTAssert(sut.daysCount() == 1)
    }
    
    func testFindsTaskByIndexPath() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [todayTask, tomorrowTask, yesterdayTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let fetchedTask = sut.task(atIndexPath: IndexPath(item: 0, section: 1))
        
        XCTAssert(fetchedTask == todayTask)
    }
    
    func testShouldCountDaysWhenSeveralDifferentDayTasksAdded() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [todayTask, todayTask, tomorrowTask, yesterdayTask, yesterdayTask, yesterdayTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        XCTAssert(sut.daysCount() == 3)
    }
    
    func testShouldSortTasksDescendingByDate() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [todayTask, tomorrowTask, yesterdayTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let task1 = sut.task(atIndex: 0)
        let task2 = sut.task(atIndex: 1)
        let task3 = sut.task(atIndex: 2)
        
        XCTAssert(task1 == tomorrowTask)
        XCTAssert(task2 == todayTask)
        XCTAssert(task3 == yesterdayTask)
    }
    
    func testShouldSortAndUpdateDaysCountAfterAddingNewTask() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let storage = StorageStub(data: [yesterdayTask, todayTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        let tomorrowTask = self.makeTomorrowTask()
        sut.add(task: tomorrowTask)
        
        XCTAssert(sut.daysCount() == 3)
        XCTAssert(sut.task(atIndex: 0) == tomorrowTask)
    }
    
    func testShouldUpdateDaysCountAfterDeletingTask() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [yesterdayTask, todayTask, tomorrowTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        sut.removeTask(atIndex: 0)
        
        XCTAssert(sut.daysCount() == 2)
    }
    
    func testShouldSortUpdateDaysCountAfterReplacingTask() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [yesterdayTask, todayTask, tomorrowTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        sut.replace(task: yesterdayTask, atIndex: 0)
        
        XCTAssert(sut.daysCount() == 2)
        XCTAssert(sut.task(atIndex: 0) == todayTask)
    }
    
    func testShouldKeepDataRepresentationWithMultipleTasksInADay() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let storage = StorageStub(data: [yesterdayTask, yesterdayTask, yesterdayTask, todayTask, todayTask, tomorrowTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 0)) == tomorrowTask)
        
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 1)) == todayTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 1, section: 1)) == todayTask)
        
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 2)) == yesterdayTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 1, section: 2)) == yesterdayTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 2, section: 2)) == yesterdayTask)
    }
    
    func testShouldKeepDataRepresentationWithDistantTasks() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let longAgoTask = self.makeLongAgoTask()
        let futureTask = self.makeFutureTask()
        let storage = StorageStub(data: [yesterdayTask, futureTask, todayTask, longAgoTask, tomorrowTask])
        let sut = TaskDataSource(storage: StorageProtocolThunk(storage))
        
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 0)) == futureTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 1)) == tomorrowTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 2)) == todayTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 3)) == yesterdayTask)
        XCTAssert(sut.task(atIndexPath: IndexPath(row: 0, section: 4)) == longAgoTask)
    }
    
    func testPersistenceShouldKeepSingleTask() {
        let todayTask = self.makeTodayTask()
        let sut = self.persistentSut()
        sut.add(task: todayTask)
        
        let otherSut = self.persistentSut()
        let readTask = otherSut.task(atIndex: 0)
        
        XCTAssert(readTask == todayTask)
    }
    
    func testPersistenceShouldKeepMultipleTask() {
        let todayTask = self.makeTodayTask()
        let yesterdayTask = self.makeYesterdayTask()
        let tomorrowTask = self.makeTomorrowTask()
        let sut = self.persistentSut()
        sut.add(task: todayTask)
        sut.add(task: yesterdayTask)
        sut.add(task: tomorrowTask)
        
        let otherSut = self.persistentSut()
        let readTask1 = otherSut.task(atIndex: 0)
        let readTask2 = otherSut.task(atIndex: 1)
        let readTask3 = otherSut.task(atIndex: 2)
        
        XCTAssert(readTask1 == tomorrowTask)
        XCTAssert(readTask2 == todayTask)
        XCTAssert(readTask3 == yesterdayTask)
    }
}
