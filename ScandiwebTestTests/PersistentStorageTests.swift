//
//  PersistentStorageTests.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 24/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import XCTest
@testable import ScandiwebTest

class PersistentStorageTests: XCTestCase {
    
    let sut = PersistentStorage<String>(fileName: "test_file")
    
    override func tearDown() {
        sut.deleteData()
        super.tearDown()
    }
    
    func testShouldBeEmptyByDefault() {
        let storedObject = sut.readData()
        XCTAssert(storedObject.isEmpty)
    }
    
    func testDeletesData() {
        let obj1 = "1"
        self.sut.save(data: [obj1])
        
        self.sut.deleteData()
        
        let retrievedData = self.sut.readData()
        XCTAssert(retrievedData.isEmpty)
        
    }
    
    func testStoreAndRetrieveData() {
        let obj1 = "1"
        let obj2 = "2"
        let obj3 = "3"
        self.sut.save(data: [obj1, obj2, obj3])
        
        let expectedCount = 3
        let retrievedData = self.sut.readData()
        XCTAssert(retrievedData.count == expectedCount, "Object count \(retrievedData.count), expected \(expectedCount). Aborting test to avoid exception")
        if (retrievedData.count == expectedCount) {
            XCTAssert(retrievedData[0] == obj1)
            XCTAssert(retrievedData[1] == obj2)
            XCTAssert(retrievedData[2] == obj3)
        }
    }
    
}
