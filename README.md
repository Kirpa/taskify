# Taskify 1.0

### General

Test project, see taks details in 'Test for iOS developer.pdf'. Universal app, compatible with iPhone and iPad. Tested on iOS 9 and 10

##### Requirements

* Xcode 8
* Cocoapods 1.1.0.rc.2 or later

##### Credits

* Button icons created by Kevin from Noun project
* App icon is by Arthur Bauer from iOS icons on Uplabs
