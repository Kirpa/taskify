//
//  TaskDataSource.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 24/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import Foundation
import UIKit

typealias TasksByDay = [[TaskModel]]

@objc
class TaskDataSource: NSObject, UITableViewDataSource {
    
    // Getting current calendar is slow, caching it
    private let cachedCalendar = NSCalendar.current
    private var tableViewData = TasksByDay()
    private var tasks: [TaskModel]
    private var storage: StorageProtocolThunk<TaskModel>
    private let headerDateFormatter: DateFormatter
    
    // MARK: Object lifecycle
    
    required init(storage: StorageProtocolThunk<TaskModel>) {
        self.storage = storage
        self.tasks = self.storage.readData()
        self.headerDateFormatter = DateFormatter()
        self.headerDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        self.headerDateFormatter.dateFormat = "d MMM, yyyy"
        self.headerDateFormatter.timeZone = NSTimeZone.system

        super.init()
        self.updateDataRepresentation()
    }
    
    convenience init(taskStorageFile: String) {
        let taskStorage = PersistentStorage<TaskModel>(fileName: taskStorageFile)
        self.init(storage: StorageProtocolThunk(taskStorage))
    }
    
    // MARK: TableView data source
    
    func tableView(_ tableView: UITableView, cellForRowAt: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCellId", for: cellForRowAt) as! TaskCell
        let task = self.task(atIndexPath: cellForRowAt)
        cell.displaySummary(task.summary)
        cell.displayDuration(task.duration)
        cell.display(task.date as Date!)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.daysCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewData[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let task = self.task(atIndexPath: IndexPath(item: 0, section: section))
        return self.headerDateFormatter.string(from: task.date as Date)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.removeTask(atIndexPath: indexPath)
            tableView.reloadData()
        }
    }
    
    // MARK: CRUD
    
    func task(atIndexPath: IndexPath) -> TaskModel {
        // drop range checking as it will introduce optionals, branches and need to notify user if element is not found
        // don't want this for test task, but will need it in production
        
        return self.tableViewData[atIndexPath.section][atIndexPath.row]
    }
    
    func task(atIndex: Int) -> TaskModel {
        return self.tasks[atIndex]
    }
    
    func add(task: TaskModel) {
        self.tasks.append(task)
        // Saving on a main thread since it's lighweight. Should use dedicated queue otherwise 
        self.storage.save(data: self.tasks)
        self.updateDataRepresentation()
    }
    
    func removeTask(atIndexPath: IndexPath) {
        let task = self.task(atIndexPath: atIndexPath)
        if let taskIndex = self.indexForTask(task: task) {
            self.removeTask(atIndex: taskIndex)
        } else {
            print("Something gone wrong while trying to delete task. Cannot find index for task: \(task)")
        }
    }
    
    func removeTask(atIndex: Int) {
        self.tasks.remove(at: atIndex)
        self.storage.save(data: self.tasks)
        self.updateDataRepresentation()
    }
    
    func replace(task: TaskModel, atIndexPath: IndexPath) {
        let oldTask = self.task(atIndexPath: atIndexPath)
        if let taskIndex = self.indexForTask(task: oldTask) {
            self.replace(task:task, atIndex: taskIndex)
        } else {
            print("Something gone wrong while trying to replace task. Cannot find index for task: \(task)")
        }
    }
    
    func replace(task: TaskModel, atIndex: Int) {
        self.removeTask(atIndex: atIndex)
        self.add(task: task)
        self.storage.save(data: self.tasks)
        self.updateDataRepresentation()
    }
    
    // MARK: Helpers
    
    func taskCount() -> Int {
        return self.tasks.count
    }
    
    func daysCount() -> Int {
        return self.tableViewData.count
    }
    
    func deletePersistentData() {
        self.storage.deleteData()
    }
    
    func deleteAllData() {
        self.tasks.removeAll()
        self.updateDataRepresentation()
    }
    
    private func indexForTask(task: TaskModel) -> Int? {
        for (i, enumeratedTask) in self.tasks.enumerated() {
            if enumeratedTask == task {
                return i
            }
        }
        
        return nil
    }
    
    private func updateDataRepresentation() {
        self.tasks = self.sortedTasks(tasks: self.tasks)
        self.tableViewData = self.makeDailyRepresentation(tasks: self.tasks)
    }
    
    private func makeDailyRepresentation(tasks: [TaskModel]) -> TasksByDay {
        func hasDifferentDates(task1: TaskModel, task2: TaskModel) -> Bool {
            return !self.cachedCalendar.isDate(task1.date as Date, inSameDayAs: task2.date as Date)
        }
        
        var result = TasksByDay()
        guard let firstTask = tasks.first else {
            return result
        }
        result.append([TaskModel]())
        result[0].append(firstTask)
        
        for (i) in tasks.indices.dropFirst() {
            let currentTask = tasks[i]
            let previousTask = tasks[i - 1]
            if hasDifferentDates(task1: currentTask, task2: previousTask) {
                result.append([TaskModel]())
            }
            
            result[result.count - 1].append(currentTask)
        }
        
        return result
    }
        
    private func sortedTasks(tasks: [TaskModel]) -> [TaskModel] {
        return self.tasks.sorted {
            $0.date.compare($1.date as (Date)) == .orderedDescending
        }
    }    
}
