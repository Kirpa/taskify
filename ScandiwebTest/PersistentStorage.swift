//
//  PersistentStorage.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 24/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import Foundation

protocol Storage {
    associatedtype ElementType
    func readData() -> [ElementType]
    func save(data: [ElementType])
    func deleteData()
}

class StorageProtocolThunk<T>: Storage {
    
    private let _readData: () -> [T]
    private let _save: ([T]) -> Void
    private let _deleteData: () -> Void
    
    init<P: Storage>(_ dep: P) where P.ElementType == T {
        _readData = dep.readData
        _save = dep.save
        _deleteData = dep.deleteData
    }
    
    func readData() -> [T] {
        return _readData()
    }
    
    func save(data: [T]) {
        return _save(data)
    }
    
    func deleteData() {
        return _deleteData()
    }
}

class PersistentStorage<T>: Storage {
 
    typealias ElementType = T
    let fileName: String
    
    required init(fileName: String) {
        self.fileName = fileName
    }
    
    private func makeURL() -> URL {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentDirectory.appendingPathComponent(self.fileName)
    }
    
    func readData() -> [T] {
        let url = self.makeURL()
        guard let storedObject = NSKeyedUnarchiver.unarchiveObject(withFile: url.path) as? [T]
            else { return Array<T>()}
        
        return storedObject
    }
    
    func save(data: [T]) {
        let url = self.makeURL()
        if (!NSKeyedArchiver.archiveRootObject(data, toFile: url.path)) {
            print("Error saving data to file: \(url.path)")
        }
    }
    
    func deleteData() {
        let url = self.makeURL()
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print ("Error deleting storage after test: \(error)")
        }
    }
}
