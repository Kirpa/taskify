//
//  UIColor+Extension.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 29/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "UIColor+Extension.h"

@implementation UIColor (Extension)

+ (UIColor *)cellHeaderBackground
{
    return [UIColor colorWithRed:154. / 255 green:209. / 255 blue:245. / 255 alpha:1.];
}

+ (UIColor *)cellHeaderText
{
    return [UIColor colorWithRed:2. / 255 green:146. / 255 blue:239. / 255 alpha:1.];
}

@end
