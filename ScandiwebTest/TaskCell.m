//
//  TaskCell.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 27/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "TaskCell.h"
#import "EditTaskView.h"
#import "NSString+Formatting.h"

@interface TaskCell()

@property (nonatomic) IBOutlet UILabel *summaryLabel;
@property (nonatomic) IBOutlet UILabel *durationLabel;
@property (nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic) IBOutlet UIStackView *stackView;
@property (nonatomic) EditTaskView *editorView;

@end

@implementation TaskCell

#pragma mark - Overrides

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self removeEditorView];
}

#pragma mark - UI update

- (void)displaySummary:(NSString *)summary
{
    self.summaryLabel.text = summary;
}

- (void)displayDuration:(NSNumber *)duration
{
    self.durationLabel.text = [NSString formattedAsHoursAndMinutes:duration];
}

- (void)displayDate:(NSDate *)date
{
    // Will not display date as tasks are grouped by dates and it's in section's header
    // This way displaying the date in each task cell is excessive and pollutes UI
    // If you want to display it anyway, uncomment the code below and check 'Installed'
    // for date label and it's leading constraint to summary label
    
//    static NSDateFormatter *formatter;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        formatter = [[NSDateFormatter alloc] init];
//        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
//        formatter.dateFormat = @"EE, MMM d, yyyy";
//        formatter.timeZone = [NSTimeZone systemTimeZone];
//    });
//    
//    self.dateLabel.text = [formatter stringFromDate:date];
}

- (void)displayEditorView:(EditTaskView *)editor
{
    [self removeEditorView];
    [self.stackView addArrangedSubview:editor];
    self.editorView = editor;
}

- (void)removeEditorView
{
    if (self.editorView) {
        [self.stackView removeArrangedSubview:self.editorView];
        [self.editorView prepareToHide];
        [self.editorView removeFromSuperview];
        self.editorView = nil;
    }
}

@end
