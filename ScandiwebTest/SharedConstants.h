//
//  SharedConstants.h
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 03/10/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#ifndef SharedConstants_h
#define SharedConstants_h

static NSString *const kIsUITestingFlag = @"UI_TESTING_ENVIRONMENT";

#endif /* SharedConstants_h */
