//
//  NSString+Formatting.h
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 28/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Formatting)

+ (NSString *)formattedAsHoursAndMinutes:(NSNumber *)minutes;

@end
