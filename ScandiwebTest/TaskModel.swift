//
//  TaskModel.swift
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 24/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

import Foundation

private let kSummaryKey = "summaryKey"
private let kDateKey = "dateKey"
private let kDurationKey = "durationKey"

@objc
class TaskModel: NSObject, NSCoding {
    var summary: String
    var date: NSDate
    var duration: NSNumber
    
    // MARK: Object lifecycle
    init(summary: String, date: NSDate, duration: NSNumber) {
        self.summary = summary
        self.date = date
        self.duration = duration
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        guard let aSummary = decoder.decodeObject(forKey: kSummaryKey) as? String,
            let aDate = decoder.decodeObject(forKey: kDateKey) as? NSDate,
            let aDuration = decoder.decodeObject(forKey: kDurationKey) as? NSNumber
            else { return nil }
        
        self.init(summary: aSummary, date: aDate, duration: aDuration)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.summary, forKey: kSummaryKey)
        aCoder.encode(self.date, forKey: kDateKey)
        aCoder.encode(self.duration, forKey: kDurationKey)
    }
    
    // MARK: Equatable
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? TaskModel else { return false }
        return self == other
    }
    
}

// MARK: Equatable implementation

func ==(lhs: TaskModel, rhs: TaskModel) -> Bool {    
    return lhs.summary == rhs.summary && lhs.date == rhs.date && lhs.duration == rhs.duration
}
