//
//  UIColor+Extension.h
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 29/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)

+ (UIColor *)cellHeaderBackground;
+ (UIColor *)cellHeaderText;

@end
