//
//  UIView+Effects.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 29/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "UIView+Effects.h"

@implementation UIView (Effects)

- (void)shake
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.duration = 0.07;
    animation.repeatCount = 3;
    animation.autoreverses = YES;
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x - 10, self.center.y)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x + 10, self.center.y)];
    [self.layer addAnimation:animation forKey:@"position"];
}

//let animation = CABasicAnimation(keyPath: "position")
//animation.duration = 0.07
//animation.repeatCount = 3
//animation.autoreverses = true
//animation.fromValue = NSValue(CGPoint: CGPointMake(self.center.x - 10, self.center.y))
//animation.toValue = NSValue(CGPoint: CGPointMake(self.center.x + 10, self.center.y))
//self.layer.addAnimation(animation, forKey: "position")

@end
