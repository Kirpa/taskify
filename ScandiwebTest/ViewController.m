//
//  ViewController.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 22/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "ViewController.h"
#import "EditTaskView.h"
#import "UIColor+Extension.h"
#import "TaskCell.h"
#import "SharedConstants.h"
#import "ScandiwebTest-Swift.h"
@import DZNEmptyDataSet;

@interface ViewController () <UITableViewDelegate, DZNEmptyDataSetSource>

@property (nonatomic) IBOutlet NSLayoutConstraint *editorViewTopConstraint;
@property (nonatomic) IBOutlet NSLayoutConstraint *topButtonToSuperviewConstraint;
@property (nonatomic) IBOutlet NSLayoutConstraint *topButtonToEditorConstraint;
@property (nonatomic) IBOutlet UIButton *showEditorButton;
@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet EditTaskView *editorView;
@property (nonatomic) TaskDataSource *taskDataSource;
@property (nonatomic) BOOL isEditorOpened;

@end

static NSString *const kStorageFileName             = @"task_storage_file";
static NSString *const kUITestsStorageFileName      = @"ui_test_task_storage_file";
static const CGFloat klayoutAnimationDuration       = 0.3;
static const CGFloat kEstimatedRowHeight            = 67.0;

@implementation ViewController

#pragma mark - Object lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createTaskDataSource];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = kEstimatedRowHeight;
    self.tableView.emptyDataSetSource = self;
    
    [self.editorView setCloseButtonVisible:NO];
    __weak typeof(self) weakSelf = self;
    self.editorView.taskCreated = ^(TaskModel *task) {
        [weakSelf userSubmittedNewTask:task];
    };
}

- (void)createTaskDataSource
{
    NSArray *processArguments = [[NSProcessInfo processInfo] arguments];
    BOOL isUITesting = [processArguments containsObject:kIsUITestingFlag];
    if (isUITesting) {
        // We are loaded in UI testing environment, make sure that data is empty
        self.taskDataSource = [[TaskDataSource alloc] initWithTaskStorageFile: kUITestsStorageFileName];
        [self.taskDataSource deleteAllData];
    } else {
        self.taskDataSource = [[TaskDataSource alloc] initWithTaskStorageFile: kStorageFileName];
    }
    self.tableView.dataSource = self.taskDataSource;
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.isEditorOpened) {
        [self showEditor:NO];
    } else {
        [self hideEditor:NO];
    }
}

#pragma mark - User interaction

- (IBAction)topButtonTouched:(id)sender
{
    if ((self.isEditorOpened)) {
        [self hideEditor:YES];
    } else {
        NSIndexPath *selectedIndex = [self.tableView indexPathForSelectedRow];
        if (selectedIndex) {
            TaskCell *cell = (TaskCell *)[self.tableView cellForRowAtIndexPath:selectedIndex];
            [self.tableView deselectRowAtIndexPath:selectedIndex animated:NO];
            [self removeEditorForCell:cell];
        }
        [self showEditor:YES];
    }
}

#pragma mark - TableView delegate

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)view;
    headerView.contentView.backgroundColor = [UIColor cellHeaderBackground];
    CGFloat fontSize = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular ? 26.0f : 17.0f;
    headerView.textLabel.font = [UIFont systemFontOfSize:fontSize];
    headerView.textLabel.textColor = [UIColor cellHeaderText];
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak TaskCell *cell = (TaskCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (cell.isSelected) {
        return indexPath;
    }
    
    if (self.isEditorOpened) {
        [self hideEditor:YES];
    }
    
    TaskModel *task = [self.taskDataSource taskAtIndexPath:indexPath];
    EditTaskView *editorView = [self makeCellEditorViewWithTask:task];
    __weak typeof(self) weakSelf = self;
    __weak typeof(tableView) weakTableView = tableView;
    editorView.taskCreated = ^(TaskModel *editedTask) {
        [weakSelf.taskDataSource replaceWithTask:editedTask atIndexPath:indexPath];
        [weakTableView reloadData];
        [weakTableView beginUpdates];
        [cell removeEditorView];
        [weakTableView endUpdates];
    };
    
    editorView.closeButtonBlock = ^() {
        [weakTableView beginUpdates];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [cell removeEditorView];
        [weakTableView endUpdates];
    };
    
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [tableView beginUpdates];
    [cell displayEditorView:editorView];
    [tableView endUpdates];
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskCell *cell = (TaskCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self removeEditorForCell:cell];
}
#pragma mark - Empty state

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Add some tasks";
    
    CGFloat fontSize = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular ? 27.0f : 18.0f;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:fontSize],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Press button at the top to add new task. Note that time spent should be entered in minutes."
    " Fill description and task date. Swipe task to delete. Tap task to edit.";
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    CGFloat fontSize = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular ? 21.0f : 14.0f;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Interface and layout

- (void)showEditor:(BOOL)animated
{
    self.isEditorOpened = YES;
    [self.showEditorButton setImage:[UIImage imageNamed:@"arrow_up"] forState:UIControlStateNormal];
    self.editorViewTopConstraint.constant = 0;
    self.topButtonToSuperviewConstraint.active = NO;
    self.topButtonToEditorConstraint.active = YES;
    [self.editorView prepareToShow];
    if (animated) {
        [self animateLayout];
    } else {
        [self.view layoutIfNeeded];
    }
}

- (void)hideEditor:(BOOL)animated
{
    self.isEditorOpened = NO;
    [self.showEditorButton setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    self.editorViewTopConstraint.constant = -self.editorView.bounds.size.height;
    self.topButtonToSuperviewConstraint.active = YES;
    self.topButtonToEditorConstraint.active = NO;
    [self.editorView prepareToHide];
    if (animated) {
        [self animateLayout];
    } else {
        [self.view layoutIfNeeded];
    }
}

- (void)animateLayout
{
    [UIView animateWithDuration:klayoutAnimationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Cell editing

- (EditTaskView *)makeCellEditorViewWithTask:(TaskModel *)task
{
    EditTaskView *view = [[EditTaskView alloc] init];
    [view displayTaskData:task];    
    return view;
}

- (void)removeEditorForCell:(TaskCell *)cell
{
    [self.tableView beginUpdates];
    [cell removeEditorView];
    [self.tableView endUpdates];
}

#pragma mark - Task creation

- (void)userSubmittedNewTask:(TaskModel *)task
{
    [self.taskDataSource addWithTask:task];
    [self.tableView reloadData];
    [self hideEditor:YES];
}

@end
