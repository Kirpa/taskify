//
//  EditTaskView.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 25/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "EditTaskView.h"
#import "NSString+Formatting.h"
#import "UIView+Effects.h"
#import "ScandiwebTest-Swift.h"
@import TextFieldEffects;

@interface EditTaskView() <UITextFieldDelegate>

@property (nonatomic) IBOutlet UIButton *confirmButton;
@property (nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic) IBOutlet YoshikoTextField *summaryTextField;
@property (nonatomic) IBOutlet YoshikoTextField *timeSpentTextField;
@property (nonatomic) IBOutlet YoshikoTextField *dateTextField;

@property (nonatomic) NSDate *date;
@property (nonatomic) NSDateFormatter *dateFormatter;
@property (nonatomic) NSNumber *timeSpent;

@end

@implementation EditTaskView

#pragma mark - Object lifecycle

- (instancetype)init
{
    if ((self = [super init])) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self commonInit];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [self loadFromNib];
    self.dateFormatter = [[NSDateFormatter alloc] init];
    
    // There should be current user locale in production code, but it requires testing
    // since some of locales might result date string being too long for UI
    // skipping as it's a test app
    self.dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    self.dateFormatter.dateFormat = @"EE, d MMM, yyyy";
    self.dateFormatter.timeZone = [NSTimeZone systemTimeZone];
}

#pragma mark - Loading from nib

- (void)loadFromNib
{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSArray *loadedViews = [mainBundle loadNibNamed:@"EditTaskView" owner:self options:nil];
    EditTaskView *loadedSubview = [loadedViews firstObject];
    
    [self addSubview:loadedSubview];
    
    loadedSubview.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeTop]];
    [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeLeft]];
    [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeBottom]];
    [self addConstraint:[self pin:loadedSubview attribute:NSLayoutAttributeRight]];
}

- (NSLayoutConstraint *)pin:(id)item attribute:(NSLayoutAttribute)attribute
{
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:attribute
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:item
                                        attribute:attribute
                                       multiplier:1.0
                                         constant:0.0];
}

#pragma mark - Public methods

- (void)setConfirmButtonVisible:(BOOL)isVisible
{
    self.confirmButton.hidden = !isVisible;
}

- (void)setCloseButtonVisible:(BOOL)isVisible
{
    self.closeButton.hidden = !isVisible;
}

- (void)prepareToHide
{
    [self endEditing:YES];
}

- (void)prepareToShow
{
    if (!self.date) {
        self.date = [NSDate date];
        [self updateDateField];
    }
}

#pragma mark - UI

- (void)clearInput
{
    self.summaryTextField.text = @"";
    self.timeSpentTextField.text = @"";
    self.timeSpent = nil;
    self.date = nil;
}

- (void)displayTaskData:(TaskModel *)task
{
    self.summaryTextField.text = task.summary;
    self.date = task.date;
    self.timeSpent = task.duration;
    [self showTimeSpentAsFormatted];
    [self updateDateField];
}

#pragma mark - Making TaskModel

- (TaskModel *)makeTaskModel
{
    TaskModel *result = [[TaskModel alloc] initWithSummary:self.summaryTextField.text date:self.date duration:self.timeSpent];
    return result;
}

- (IBAction)confirmButtonTouched
{
    if (self.summaryTextField.text.length == 0) {
        [self.summaryTextField shake];
        return;
    }
    
    if (self.timeSpentTextField.text.length == 0) {
        [self.timeSpentTextField shake];
        return;
    }
    
    [self endEditing:YES];
    
    if (self.taskCreated) {
        self.taskCreated([self makeTaskModel]);
        [self clearInput];
    }
}

- (IBAction)closeButtonTouched
{
    if (self.closeButtonBlock) {
        self.closeButtonBlock();
    }
}

#pragma mark - Date

- (void)updateDateField
{
    self.dateTextField.text = [self.dateFormatter stringFromDate:self.date];
}

- (void)showDateSelectionForView:(UITextField *)sender
{
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    picker.date = self.date;
    picker.datePickerMode = UIDatePickerModeDate;
    sender.inputView = picker;
    [picker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)datePickerChanged:(UIDatePicker *)sender
{
    self.date = sender.date;
    [self updateDateField];
    [self.dateTextField resignFirstResponder];
}

#pragma mark - Duration

- (void)showTimeSpentAsFormatted
{
    self.timeSpentTextField.text = [NSString formattedAsHoursAndMinutes:self.timeSpent];
}

- (void)showTimeSpentAsMinutes
{
    self.timeSpentTextField.text = [self.timeSpent stringValue];
}

- (void)updateTimeSpentValue
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.timeSpent = [formatter numberFromString:self.timeSpentTextField.text];
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.dateTextField) {
        [self showDateSelectionForView:textField];
    } else if (textField == self.timeSpentTextField) {
        [self showTimeSpentAsMinutes];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{    
    if (textField == self.timeSpentTextField && self.timeSpentTextField.text.length > 0) {
        [self updateTimeSpentValue];
        [self showTimeSpentAsFormatted];
    }
}

@end
