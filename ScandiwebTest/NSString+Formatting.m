//
//  NSString+Formatting.m
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 28/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import "NSString+Formatting.h"

@implementation NSString (Formatting)

+ (NSString *)formattedAsHoursAndMinutes:(NSNumber *)minutes
{
    static NSDateComponentsFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateComponentsFormatter alloc] init];
        formatter.unitsStyle = NSDateComponentsFormatterUnitsStylePositional;
        formatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorNone;
        formatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute;
    });
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.minute = [minutes integerValue];
    return [formatter stringFromDateComponents:components];
}

@end
