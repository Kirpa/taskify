//
//  TaskCell.h
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 27/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditTaskView;

@interface TaskCell : UITableViewCell

- (void)displaySummary:(NSString *)summary;
- (void)displayDuration:(NSNumber *)duration;
- (void)displayDate:(NSDate *)date;
- (void)displayEditorView:(EditTaskView *)editor;
- (void)removeEditorView;

@end
