//
//  EditTaskView.h
//  ScandiwebTest
//
//  Created by Vadim Zhepetov on 25/09/16.
//  Copyright © 2016 Vadim Zhepetov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TaskModel;

typedef void(^TaskCreatedBlock)(TaskModel * _Nonnull);

@interface EditTaskView : UIView

@property (nonatomic, copy, nullable) TaskCreatedBlock taskCreated;
@property (nonatomic, copy, nullable) void (^closeButtonBlock)();

- (void)displayTaskData:(nonnull TaskModel *)task;
- (void)setConfirmButtonVisible:(BOOL)isVisible;
- (void)setCloseButtonVisible:(BOOL)isVisible;
- (void)prepareToHide;
- (void)prepareToShow;

@end
